//consumidor
package Pizzaria;

public class Motoqueiro extends Thread
{
	private Pizzafina pizzafina;
	
	public Motoqueiro(Pizzafina pizzafina){
		this.pizzafina = pizzafina;
	}
	
	public void run(){
		try{
			while(pizzafina.getNumPizzasTotal()<500) {
				pizzafina.levantarPizza();
				sleep((long) (15+Math.random()*15));
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
