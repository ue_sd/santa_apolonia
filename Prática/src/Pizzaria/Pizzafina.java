//Buffer
package Pizzaria;

import java.util.ArrayList;

public class Pizzafina {
	private int numPizzas;
	private int numPizzasTotal;
	private final int CAPACIDADE = 10;
	private final int NUM_PIZZEIROS = 3;
	private final int NUM_MOTOQUEIROS = 10;
	private ArrayList<Pizzeiro> pizzeiros;
	private ArrayList<Motoqueiro> motoqueiros;
	
	public Pizzafina() {
		numPizzas = 0;
		numPizzasTotal = 0;
		pizzeiros = new ArrayList<Pizzeiro>();
		motoqueiros = new ArrayList<Motoqueiro>();
		
		for (int i = 0; i < NUM_PIZZEIROS; i++) {
			pizzeiros.add(new Pizzeiro(this));	
		}
		
		for (int i = 0; i < NUM_MOTOQUEIROS; i++) {
			motoqueiros.add(new Motoqueiro(this));	
		}						
	}

	private void arrancaDiaDeTrabalho() {
		for (Motoqueiro motoqueiro : motoqueiros) {
			motoqueiro.start();
		}
		
		for (Pizzeiro pizzeiro : pizzeiros) {
			pizzeiro.start();
		}
		
	}
	
	private void fecharOTasco() throws InterruptedException{
		for (Motoqueiro motoqueiro : motoqueiros) {
			motoqueiro.join();
		}
		
		for (Pizzeiro pizzeiro : pizzeiros) {
			pizzeiro.join();
		}
	}
		
		
	public static void main(String[] args) {
		Pizzafina p = new Pizzafina();
		p.arrancaDiaDeTrabalho();
		try {
			p.fecharOTasco();
		} catch (InterruptedException e) {
		e.printStackTrace();
		}
	}
	public synchronized void colocaPizza() {
		//Produtor
		while(numPizzas == CAPACIDADE){
			try {
				wait();
			} catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
		System.out.println(Thread.currentThread()+ ": Pizza Criada");
		if(numPizzasTotal < 500)
		{
			numPizzas++;
		}
		
		numPizzasTotal++;
		if (numPizzas ==1) {
			notifyAll();
		}
	}
	
	public synchronized void levantarPizza(){
		//Consumidor
		while(numPizzas == 0){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		numPizzas--;
		System.out.println(Thread.currentThread()+ ": Pizza Entregue" + numPizzasTotal);
		if(numPizzas==CAPACIDADE-1){
				notifyAll();
			}
		}
		
		public synchronized int getNumPizzasTotal(){
			return numPizzasTotal;
		}
	}
