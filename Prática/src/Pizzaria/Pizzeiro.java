//Produtor
package Pizzaria;

public class Pizzeiro extends Thread
{
		private Pizzafina pizzafina;
	
		public Pizzeiro(Pizzafina pizzafina){
			this.pizzafina = pizzafina;
		}
	
		public void run() {
			try {
				while(pizzafina.getNumPizzasTotal()<500) {
				sleep(30);
				pizzafina.colocaPizza();
				}
			} catch (InterruptedException e){
			e.printStackTrace();
		}
	}
}
